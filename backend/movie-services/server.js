const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

// add the routers
const routerCategory = require('./routes/movie')
app.use('/movie', routerCategory)

app.listen(4001, '0.0.0.0', () => {
  console.log('movie started on port 4000')
})
